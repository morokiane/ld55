extends Area2D

@onready var redBTN: TextureButton = $CanvasLayer/SphereChoice/VBoxContainer/RedBTN
@onready var greenBTN: TextureButton = $CanvasLayer/SphereChoice/VBoxContainer/GreenBTN
@onready var blueBTN: TextureButton = $CanvasLayer/SphereChoice/VBoxContainer/BlueBTN
@onready var sphereChoice: NinePatchRect = $CanvasLayer/SphereChoice
@onready var redSphere: Sprite2D = $RedSphere
@onready var greenSphere: Sprite2D = $GreenSphere
@onready var blueSphere: Sprite2D = $BlueSphere

var canUse: bool = false
var hasSphere: bool = false

@export_enum("Red", "Green", "Blue") var level: int

func _ready() -> void:
	sphereChoice.hide()
	redSphere.hide()
	greenSphere.hide()
	blueSphere.hide()

func _process(_delta: float) -> void:
	if !GameController.hasRedSphere:
		redBTN.disabled = true
		redBTN.hide()
	if !GameController.hasGreenSphere:
		greenBTN.disabled = true
		greenBTN.hide()
	if !GameController.hasBlueSphere:
		blueBTN.disabled = true
		blueBTN.hide()

func _on_body_entered(body:Node2D) -> void:
	if body is Player:
		canUse = true

func _on_body_exited(body:Node2D) -> void:
	if body is Player:
		canUse = false

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("accept") && canUse && !sphereChoice.visible:
		sphereChoice.show()
		GameController.player.canMove = false

		if GameController.hasRedSphere:
			redBTN.grab_focus()
		elif GameController.hasGreenSphere:
			greenBTN.grab_focus()
		elif GameController.hasBlueSphere:
			blueBTN.grab_focus()

	elif event.is_action_pressed("esc") && sphereChoice.visible:
		sphereChoice.hide()
		GameController.player.canMove = true

func _on_cancel_btn_pressed() -> void:
	sphereChoice.hide()
	GameController.player.canMove = true

func _on_red_btn_pressed() -> void:
	SoundFx.play("blip")
	redSphere.show()
	sphereChoice.hide()
	GameController.hasRedSphere = false
	GameController.player.canMove = true
	GameController.redPedestalActif = true

func _on_green_btn_pressed() -> void:
	SoundFx.play("blip")
	greenSphere.show()
	sphereChoice.hide()
	GameController.hasGreenSphere = false
	GameController.player.canMove = true
	GameController.greenPedestalActif = true

func _on_blue_btn_pressed() -> void:
	SoundFx.play("blip")
	blueSphere.show()
	sphereChoice.hide()
	GameController.hasBlueSphere = false
	GameController.player.canMove = true
	GameController.bluePedestalActif = true
