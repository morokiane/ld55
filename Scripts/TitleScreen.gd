extends CanvasLayer

@onready var btnPlay: TextureButton = $VBoxContainer/Play

func _ready() -> void:
	btnPlay.grab_focus()

func _on_play_pressed() -> void:
	SoundFx.play("blip")
	get_tree().change_scene_to_file("res://Levels/InitialLevel.scn")

func _on_quit_pressed() -> void:
	SoundFx.play("blip")
	get_tree().quit()
