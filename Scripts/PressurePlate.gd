extends StaticBody2D

signal turnOn
signal turnOff

@onready var detectorL: RayCast2D = $DetectorL
@onready var detectorR: RayCast2D = $DetectorR
@onready var sprite: Sprite2D = $Sprite2D

@export var oneTime: bool = false

# func _ready():
# 	object = get_tree().get_nodes_in_group("trigger")
# 	for child in object:
# 		child.connect("turnOn", child)
# 		child.connect("turnOff", child)

func _process(_delta):
	if detectorL.is_colliding() || detectorR.is_colliding():
		sprite.frame = 1
		emit_signal("turnOn")
	elif oneTime:
		return
	else:
		sprite.frame = 0
		emit_signal("turnOff")
