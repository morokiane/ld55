extends Area2D

@onready var anim: AnimationPlayer = $AnimationPlayer
@onready var coll: CollisionShape2D = $CollisionShape2D

var retracted: bool = false

func _ready():
	if get_parent().is_in_group("switch"):
		get_parent().connect("turnOn", TurnOn)
		get_parent().connect("turnOff", TurnOff)
		
	anim.play("idle")

func _on_body_entered(body):
	if body is Player:
		GameController.player.Flash()
		if GameController.playerHP > 0:
			SoundFx.play("playerhit")
			GameController.playerHP -= 1
			GameController.player.canGetHit = false

func _on_body_exited(body:Node2D) -> void:
	if body is Player:
		GameController.player.canGetHit = true

func TurnOff():
	if retracted:
		SoundFx.play("push")
		retracted = false
		anim.play("detract")

func TurnOn():
	if !retracted:
		SoundFx.play("push")
		anim.play("retract")
		retracted = true

