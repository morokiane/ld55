extends CharacterBody2D
class_name Player

enum {
	move,
	passedout
}

@onready var sprite: Sprite2D = $Sprite2D
@onready var anim: AnimationPlayer = $AnimationPlayer
@onready var hurtbox: CollisionShape2D = $Hurtbox/CollisionShape2D
@onready var flashTimer: Timer = $FlashTimer
#@onready var invins: Timer = $InvisTimer
@onready var collision: CollisionPolygon2D = $CollisionPolygon2D
@onready var cam: Camera2D = get_node("../Camera2D")
@onready var stuck: RayCast2D = $RayCast2D

@export var moveSpeed: float = 80
@export var accel: float = 900
@export var frict: float = 600
@export var jheight: float = -85
@export var jrelease: float = -70
@export var fallSpeed: float = 3
@export var pushForce: float = 20
#@export var grav: float = 200
var grav = ProjectSettings.get_setting("physics/2d/default_gravity")

var lastPosition: Vector2 = Vector2.ZERO
var state = move
var doubleJump: int = 1
var canGetHit: bool = true
var canMove: bool = true

func _ready():
	GameController.player = self
#	hurtbox.disabled = true
# warning-ignore:return_value_discarded
#	GameController.connect("revive",Callable(self,"StartRevive"))

func _physics_process(delta):
	if stuck.is_colliding() && sprite.flip_h:
		position.x -= 4
	elif stuck.is_colliding() && !sprite.flip_h:
		position.x += 4
	
	var input = Vector2.ZERO
	input.x = Input.get_axis("left", "right")
	input.y = Input.get_axis("up", "down")

#	if Input.is_action_just_pressed("A") && GameController.playerHP == 0:
#		GameController.emit_signal("revive")
	# for i in get_slide_collision_count():
	# 	var c = get_slide_collision(i)
	# 	if c.get_collider() is RigidBody2D:
	# 		c.get_collider().apply_central_impulse(-c.get_normal() * pushForce)

	if canMove:
		match state:
			move: Move(input, delta)

	# if GameController.playerHP <= 0:
	# 	state = passedout

func ApplyGravity(delta):
	velocity.y += grav * delta

func ApplyFriction(delta):
	velocity.x = move_toward(velocity.x, 0, frict * delta)
	
func ApplyAccel(amount, delta):
	velocity.x = move_toward(velocity.x, moveSpeed * amount, accel * delta)
	
func Move(input, delta):
	ApplyGravity(delta)

	if input.x == 0:
		ApplyFriction(delta)
		anim.play("idle")
	else:
		ApplyAccel(input.x, delta)
		anim.play("run")
		if input.x > 0:
			sprite.flip_h = false
#			$Hurtbox.scale.x = 1
		else:
			sprite.flip_h = true
#			$Hurtbox.scale.x = -1

	if is_on_floor():
		doubleJump = GameController.doubleJump

		if Input.is_action_just_pressed("jump"):
			velocity.y = jheight
			SoundFx.play("jump")

	else:
		anim.play("jump")
		if Input.is_action_just_released("jump") && velocity.y < jrelease:
			velocity.y = jrelease

		if Input.is_action_just_pressed("jump") && doubleJump > 0:
			velocity.y = jheight
			doubleJump = 0
		
		if velocity.y > 0:
			anim.play("fall")
			velocity.y += fallSpeed

	# set_velocity(velocity)
	set_up_direction(Vector2.UP)
	move_and_slide()
	# velocity = velocity

func Flash():
	if canGetHit:
		sprite.material.set_shader_parameter("flashModifier", 2)
		flashTimer.start()
		if !sprite.flip_h:
			velocity.x = -125
			velocity.y = -50
	#		anim.play("hurt")
		elif sprite.flip_h:
			velocity.x = 125
			velocity.y = -50
	#		anim.play("hurt")

		cam.shake(0.5,90,1)
		SoundFx.play("playerhit")

func _on_flash_timer_timeout():
	sprite.material.set_shader_parameter("flashModifier", 0)
