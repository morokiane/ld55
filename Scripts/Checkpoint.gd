extends Node2D

var checkPoint: Vector2 = Vector2.ZERO

func _ready():
	checkPoint = self.global_position

func _on_Area2D_body_entered(body):
	if body is Player:
		GameController.lastLocation = checkPoint
