extends Area2D

signal turnOn
signal turnOff

@export var canTimeOut: bool = false
@export var timeOut: float = 0
@export var flip: bool = false

@onready var sprite: Sprite2D = $Sprite2D
@onready var timer: Timer = $Timer

var canUse: bool = false

func _ready():
	timer.wait_time = timeOut

	if flip:
		sprite.frame = 1

func _input(event):
	if event.is_action_pressed("accept") && canUse:
		if sprite.frame == 0:
			sprite.frame = 1
			emit_signal("turnOn")
		else:
			sprite.frame = 0
			emit_signal("turnOff")

		if canTimeOut:
			timer.start()

func _on_body_entered(body):
	if body is Player:
		canUse = true

func _on_body_exited(body):
	if body is Player:
		canUse = false

func _on_timer_timeout():
	# sprite.frame = 0
	timer.stop()

	if !flip:
		sprite.frame = 0
		emit_signal("turnOff")
	else:
		sprite.frame = 1
		emit_signal("turnOn")
