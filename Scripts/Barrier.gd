@tool
extends StaticBody2D

@onready var anim: AnimationPlayer = $AnimationPlayer
@onready var sprite: Sprite2D = $Sprite2D

@export var flipH: bool = false
@export var retracted: bool = false

func _ready():
	if get_parent().is_in_group("switch"):
		get_parent().connect("turnOn", TurnOn)
		get_parent().connect("turnOff", TurnOff)
	
	if !retracted:
		anim.play("idle")
	else:
		anim.seek(0.5)
		retracted = true

	if flipH:
		sprite.flip_h = true

func TurnOn():
	if !retracted:
		SoundFx.play("push")
		anim.play("retract")
		retracted = true

func TurnOff():
	if retracted:
		SoundFx.play("push")
		retracted = false
		anim.play("detract")
