extends Node2D

@onready var portal: AnimatedSprite2D = $AnimatedSprite2D
@onready var anim: AnimationPlayer = $AnimationPlayer
@onready var quit: TextureButton = $CanvasLayer/Info/Quit

var canEnd: bool = true

func _process(_delta: float) -> void:
	if GameController.redPedestalActif && GameController.greenPedestalActif && GameController.bluePedestalActif:
		ShowEnd()

func ShowEnd():
	if canEnd:
		anim.play("showtext")
		quit.disabled = false
		quit.grab_focus()

func DisableAnimation():
	canEnd = false

func _on_quit_pressed() -> void:
	get_tree().quit()
