extends Area2D

@onready var sprite: Sprite2D = $Sprite

const redShard: Texture2D = preload("res://Graphics/RedShard.png")
const blueShard: Texture2D = preload("res://Graphics/BlueShard.png")
const greenShard: Texture2D = preload("res://Graphics/GreenShard.png")

@export_enum("Red", "Green", "Blue") var shardType: int

func _ready() -> void:
	if shardType == 0:
		sprite.texture = redShard
	elif shardType == 1:
		sprite.texture = greenShard
	elif shardType == 2:
		sprite.texture = blueShard

func _on_body_entered(body:Node2D) -> void:
	if body is Player:
		SoundFx.play("pickup")
		GameController.lvl.shardCount += 1
		GameController.hud.shardNum.text = str(GameController.lvl.shardCount)
		queue_free()
