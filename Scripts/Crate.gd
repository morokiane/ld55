extends CharacterBody2D

@onready var sprite: Sprite2D = $Sprite
@onready var top: CollisionShape2D = $StaticBody2D/CollisionShape2D
@onready var playerDetect: CollisionShape2D = $PlayerDetect/CollisionShape2D
@onready var wallDetectR: RayCast2D = $WallDetectR
@onready var wallDetectL: RayCast2D = $WallDetectL

@export var moveable: bool = true
@export var fallSpeed: float = -12
@export var frict: float = 300

var flip: int = 0
var playerSlowed: bool = false
var canShift: bool = false
var againstWall: bool = false
var grav = ProjectSettings.get_setting("physics/2d/default_gravity")

func _ready() -> void:
	if !moveable:
		playerDetect.disabled = true

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("shift") and canShift:
		var getPosition: float
		getPosition = global_position.x - GameController.player.global_position.x

		if getPosition > 0:
			global_position.x = GameController.player.global_position.x - 13
			GameController.player.sprite.flip_h = true
		else:
			global_position.x = GameController.player.global_position.x + 11
			GameController.player.sprite.flip_h = false

		againstWall = false
		collision_layer = 2

func _process(_delta: float) -> void:
	if wallDetectR.is_colliding() || wallDetectL.is_colliding() && is_on_floor():
		againstWall = true
		collision_layer = 8

func _physics_process(delta):
	velocity.y += grav * delta
	velocity.x = move_toward(velocity.x, 0, frict * delta)

	# var friction_force = velocity.normalized() * -frict * delta
	# velocity += friction_force

	if velocity.y > 0:
		velocity.y += fallSpeed

	# if is_on_wall():
	# 	moveable = false

	set_up_direction(Vector2.UP)
	if !againstWall:
		move_and_slide()

	if moveable:
		top.disabled = true
	else:
		top.disabled = false

func _on_playerdetect_body_entered(body:Node2D):
	if body is Player:
		moveable = false

func _on_playerdetect_body_exited(body:Node2D):
	if body is Player:
		moveable = true

func _on_push_body_entered(body:Node2D):
	if body is Player:
		canShift = true
		GameController.player.moveSpeed = GameController.player.moveSpeed / 2

func _on_push_body_exited(body:Node2D):
	if body is Player:
		canShift = false
		GameController.player.moveSpeed = GameController.player.moveSpeed * 2