extends Node
var soundPath = "res://SFX/"

@onready var soundPlayers = get_children()

var sounds = {
	"pickup" : load(soundPath + "Pickup.wav"),
	"hurt" : load(soundPath + "Hurt.wav"),
	"jump" : load(soundPath + "Jump.wav"),
	"powerup" : load(soundPath + "Powerup.wav"),
	"push" : load(soundPath + "Push.wav"),
	"hit" : load(soundPath + "Hit.wav"),
	"delivered" : load(soundPath + "Delivered.wav"),
	"barrier" : load(soundPath + "Barrier.wav"),
	"playerhit" : load(soundPath + "PlayerHit.wav"),
	"pit" : load(soundPath + "Pit.wav"),
	"blip" : load(soundPath + "Blip.wav"),
	"sphere" : load(soundPath + "Sphere.wav")
}

func play(soundString):
	for soundPlayer in soundPlayers:
		if not soundPlayer.playing:
			soundPlayer.stream = sounds[soundString]
			soundPlayer.play()
			return
