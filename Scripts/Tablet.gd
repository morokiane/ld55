extends Area2D

@onready var redBTN: TextureButton = $CanvasLayer/SphereChoice/VBoxContainer/RedBTN
@onready var greenBTN: TextureButton = $CanvasLayer/SphereChoice/VBoxContainer/GreenBTN
@onready var blueBTN: TextureButton = $CanvasLayer/SphereChoice/VBoxContainer/BlueBTN
@onready var sphereChoice: NinePatchRect = $CanvasLayer/SphereChoice
@onready var info: NinePatchRect = $CanvasLayer/Info
@onready var sprite: Sprite2D = $Sprite2D
@onready var ctrl: Sprite2D = $CtrlBTN

var canUse: bool = false
var selectable: bool = true

func _ready() -> void:
	sphereChoice.hide()
	info.hide()
	ctrl.hide()
	print("Red: ", GameController.hasRedSphere)
	print("Green: ", GameController.hasGreenSphere)
	print("Blue: ", GameController.hasBlueSphere)

	if GameController.hasRedSphere:
		redBTN.disabled = true
		redBTN.hide()
	if GameController.hasGreenSphere:
		greenBTN.disabled = true
		greenBTN.hide()
	if GameController.hasBlueSphere:
		blueBTN.disabled = true
		blueBTN.hide()

func _process(_delta: float) -> void:
	if GameController.hasAllSpheres:
		sprite.frame = 1
		selectable = false

func _on_body_entered(body:Node2D) -> void:
	if body is Player && selectable:
		ctrl.show()
		canUse = true

func _on_body_exited(body:Node2D) -> void:
	if body is Player:
		ctrl.hide()
		canUse = false

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("accept") && canUse && !sphereChoice.visible:
		sphereChoice.show()
		info.show()
		if !GameController.hasRedSphere:
			redBTN.grab_focus()
		elif !GameController.hasGreenSphere:
			greenBTN.grab_focus()
		elif !GameController.hasBlueSphere:
			blueBTN.grab_focus()

		GameController.player.canMove = false
		GameController.player.anim.play("idle")

	elif event.is_action_pressed("esc") && sphereChoice.visible:
		sphereChoice.hide()
		info.hide()
		GameController.player.canMove = true

func _on_red_btn_pressed() -> void:
	SoundFx.play("blip")
	GameController.hud.trans.play("out")
	await get_tree().create_timer(1).timeout
	RenderingServer.set_default_clear_color(Color("000000"))
	get_tree().change_scene_to_file("res://Levels/Red.scn")

func _on_green_btn_pressed() -> void:
	SoundFx.play("blip")
	GameController.hud.trans.play("out")
	await get_tree().create_timer(1).timeout
	RenderingServer.set_default_clear_color(Color("000000"))
	get_tree().change_scene_to_file("res://Levels/Green.scn")

func _on_blue_btn_pressed() -> void:
	SoundFx.play("blip")
	GameController.hud.trans.play("out")
	await get_tree().create_timer(1).timeout
	RenderingServer.set_default_clear_color(Color("000000"))
	get_tree().change_scene_to_file("res://Levels/Blue.scn")
