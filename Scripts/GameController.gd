extends Node

var player: CharacterBody2D
var cam: Camera2D
var lvl: Node
var hud: CanvasLayer

var doubleJump: int
var hasRedSphere: bool = false
var hasGreenSphere: bool = false
var hasBlueSphere: bool = false
var lastLocation: Vector2
var playerHP: int = 3
var hasAllSpheres: bool = false
var redPedestalActif: bool = false
var greenPedestalActif: bool = false
var bluePedestalActif: bool = false