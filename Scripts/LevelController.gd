extends Node

@export_enum("Red", "Green", "Blue", "Non") var sphereType: int

var shardCount: int = 0

func _ready() -> void:
	GameController.lvl = self
	#sky color
	RenderingServer.set_default_clear_color(Color("249fde"))

	if GameController.hasRedSphere && GameController.hasBlueSphere && GameController.hasGreenSphere:
		GameController.hasAllSpheres = true
		print("Level Controller: ", GameController.hasAllSpheres)

	GameController.hud.trans.play("in")

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("reset"):
		shardCount = 0
		get_tree().reload_current_scene()

func _process(_delta: float) -> void:
	if shardCount == 4:
		GameController.hud.sphere.show()
		GameController.hud.anim.play("sphere")

		if GameController.player.is_on_floor():
			GameController.player.anim.play("idle")
			GameController.player.canMove = false
		
		if sphereType == 0:
			GameController.hasRedSphere = true
		if sphereType == 1:
			GameController.hasGreenSphere = true
		if sphereType == 2:
			GameController.hasBlueSphere = true

	if GameController.playerHP <= 0:
		GameController.player.canMove = true
		await get_tree().create_timer(1).timeout
		GameController.hud.trans.play("out")
		GameController.playerHP = 3
		RenderingServer.set_default_clear_color(Color("000000"))
		get_tree().change_scene_to_file("res://Levels/InitialLevel.scn")
