extends CanvasLayer

@onready var sprite: Sprite2D = $ShardFrame/Sprite
@onready var shardNum: Label = $ShardFrame/Label
@onready var sphere: Sprite2D = $Sphere
@onready var tinyRedSphere: Sprite2D = $ShardFrame/TinyRedSphere
@onready var tinyGreenSphere: Sprite2D = $ShardFrame/TinyGreenSphere
@onready var tinyBlueSphere: Sprite2D = $ShardFrame/TinyBlueSphere
@onready var anim: AnimationPlayer = $AnimationPlayer
@onready var hpBar: TextureProgressBar = $TextureProgressBar
@onready var color: ColorRect = $ColorRect
@onready var trans: AnimationPlayer = $Transition/AnimationPlayer

const redShard: Texture2D = preload("res://Graphics/RedShard.png")
const blueShard: Texture2D = preload("res://Graphics/BlueShard.png")
const greenShard: Texture2D = preload("res://Graphics/GreenShard.png")
const redSphere: Texture2D = preload("res://Graphics/LargeRedSphere.png")
const blueSphere: Texture2D = preload("res://Graphics/LargeBlueSphere.png")
const greenSphere: Texture2D = preload("res://Graphics/LargeGreenSphere.png")

@export_enum("Non", "Red", "Green", "Blue") var shardType: int

func _ready() -> void:
	GameController.hud = self
	sphere.hide()
	$Transition.show()

	if shardType == 1:
		sprite.texture = redShard
		sphere.texture = redSphere
		anim.play("showshardframe")
	elif shardType == 2:
		sprite.texture = greenShard
		sphere.texture = greenSphere
		anim.play("showshardframe")
	elif shardType == 3:
		sprite.texture = blueShard
		sphere.texture = blueSphere
		anim.play("showshardframe")

	if GameController.hasRedSphere:
		tinyRedSphere.show()
	if GameController.hasGreenSphere:
		tinyGreenSphere.show()
	if GameController.hasBlueSphere:
		tinyBlueSphere.show()

	hpBar.value = GameController.playerHP

func _process(_delta: float) -> void:
	hpBar.value = GameController.playerHP

func EndLevel():
	SoundFx.play("sphere")
	anim.active = false
	GameController.hud.trans.play("out")
	await get_tree().create_timer(1).timeout
	RenderingServer.set_default_clear_color(Color("000000"))
	get_tree().change_scene_to_file("res://Levels/InitialLevel.scn")